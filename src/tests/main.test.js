import Vue from 'vue';
import { mount } from '@vue/test-utils'
import Overview from '../components/Overview';
import AdminView from '../components/AdminView';
import App from '../App';
// import { describe, it, expect, mount } from 'jest';

describe('App', () => {
    it('goes to Admin View when changing showAdming through goAdmin Function', () => {
        const wrapper = mount(App);
        wrapper.find(Overview).vm.$emit('goAdmin');
        expect(
            wrapper.html()
        ).toContain('Director')
    })
})

describe('AdminView movies rendering data', () => {
    it('renders data when AdminView is triggered', () => {
        const wrapper = mount(AdminView, {propsData:{ movies: [{title:"test",director:"testDirector"}]}});
        // wrapper.find(Overview).vm.$emit('goAdmin');
        wrapper.find(AdminView);
        expect(
            wrapper.html()
        ).toContain('testDirector')
    })
})
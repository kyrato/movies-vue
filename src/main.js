import Vue from 'vue'
import App from './App.vue'
import { library } from '@fortawesome/fontawesome-svg-core'
import { faDatabase, faEye, faTrashAlt, faPlus, faChevronLeft, faTrash, faCheckCircle, faTimesCircle } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'

library.add(faDatabase);
library.add(faEye);
library.add(faTrashAlt);
library.add(faPlus);
library.add(faChevronLeft);
library.add(faTrash);
library.add(faCheckCircle);
library.add(faTimesCircle);

Vue.component('font-awesome-icon', FontAwesomeIcon);
Vue.config.productionTip = false;

new Vue({
  render: h => h(App),
}).$mount('#app');
